const NodeRSA = require('node-rsa');
const key = new NodeRSA({b:512});

key.generateKeyPair();

const publicDer = key.exportKey('public');
const privateDer = key.exportKey('private');

function start() {
	const str = process.argv[2];
	if(str) {
		const encrypted = key.encrypt(str, 'base64')
		console.log(`Message "${str}" encoded => ${encrypted}`);		
		const decrypted = key.decrypt(encrypted, 'utf8');
		console.log(`Message decoded back =>  "${decrypted}"`);	
	} else {
		console.log("Please send message to encrypt as first argument")
	}
}


start();
