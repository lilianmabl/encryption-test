const crypto = require('crypto');
const KEY = "symetric_key"

function encode(text, key){
	const mykey　= crypto.createCipher('aes-128-cbc', key);
	let str = mykey.update(text, 'utf8', 'hex')
	str += mykey.final('hex')

	return str;
}

function decode(message, key){
	const mykey　= crypto.createDecipher('aes-128-cbc', key);
	let str = mykey.update(message, 'hex', 'utf8');
	str += mykey.final('utf8');
	return str;
}


function start() {
	const action = process.argv[2];
	const str = process.argv[3];
	if(action === 'e') {
		console.log(`Message "${str}" encoded`);
		console.log(encode(str, KEY));
	} else if(action == 'd') {
		console.log(`Message decoded =>  "${decode(str, KEY)}"`);
	} else {
		console.log("[e/d] message")
	}
}

start();
